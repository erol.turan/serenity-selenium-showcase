# serenity-selenium-showcase

This showcase uses serenity and selenium with in gherkin written simple test cases. Cucumber is used for interpreting the gherkin files.

Combining this set of libraries enables us to create a step dictionary which is usable by everyone to automate test cases in a natural language.


## Getting Started

The project itself uses gradle-wrapper, so it only needs java to run.

Open the command line and run with following command:
```
./gradlew clean test aggregate
```
This will run all test cases and generating a report in a folder named "target". In the serenity.properties file are only a few config examples.
For further details open the serenity reference manual.

## Author

* **Erol Turan**

