Feature: Translating a given text

  To make the web accessible for everyone
  As a user
  I want to be able to understand text in an unfamiliar language


  Scenario: Translate one word
    Given the google translate page is opened
    When the input box gets the text 'wool'
    Then the result box has the text 'Wolle'

  Scenario: Translate a sentence
    Given the google translate page is opened
    When the input box gets the text 'BDD is great'
    Then the result box has the text 'BDD ist großartig'

  Scenario Outline: Translate some examples
    Given the google translate page is opened
    When the input box gets the text '<english>'
    Then the result box has the text '<german>'

    Examples:
      | english | german  |
      | horse   | Pferd   |
      | parrot  | Papagei |
      | cow     | Kuh     |
      | pig     | Hamster |