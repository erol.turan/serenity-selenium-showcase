Feature: Searching by users input

  To make the web more user-friendly
  As a user
  I want to be able to search the web for a given input


  Scenario: Searching for a specified keyword
    Given the google search page is opened
    When searching for 'wool'
    Then only sites are found related to 'wool'