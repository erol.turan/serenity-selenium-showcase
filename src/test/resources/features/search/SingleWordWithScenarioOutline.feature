Feature: Searching by users input based on a example table

  To make the web more user-friendly
  As a user
  I want to be able to search the web for a given input


  Scenario Outline: Searching for a list of specified keywords
    Given the google search page is opened
    When searching for '<keyword>'
    Then only sites are found related to '<result>'

    Examples:
      | keyword | result |
      | wool    | wool   |
      | pony    | pony   |
      | foo     | bar    |