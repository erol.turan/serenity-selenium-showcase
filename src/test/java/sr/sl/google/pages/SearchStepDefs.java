package sr.sl.google.pages;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Managed;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;

public class SearchStepDefs {

    @Managed
    WebDriver driver;

    Search search;

    @Given("the google search page is opened")
    public void theGoogleSearchPageIsOpened() {
        search.open();
    }

    @When("searching for '(.*)'")
    public void searchingFor(String query) {
        search.searchFor(query);
    }

    @Then("only sites are found related to '(.*)'")
    public void onlySitesAreFoundRelatedTo(String query) {
        Assertions.assertThat(driver.getTitle()).contains(query);
    }


}
