package sr.sl.google.pages;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Managed;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;

public class TranslateStepDefs {

    @Managed
    WebDriver driver;

    Translate translate;

    @Given("the google translate page is opened")
    public void theGoogleTranslatePageIsOpened() {
        translate.open();
    }

    @When("the input box gets the text '(.*)'")
    public void theInputBoxGetsTheText(String query) {
        translate.translate(query);
    }

    @Then("the result box has the text '(.*)'")
    public void theResultBoxHasTheText(String result) {
        Assertions.assertThat(translate.getResult()).containsIgnoringCase(result);
    }


}
