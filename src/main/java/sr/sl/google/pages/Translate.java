package sr.sl.google.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;


@DefaultUrl("https://translate.google.de/")
public class Translate extends PageObject {

    @FindBy(id = "source")
    WebElement source;

    @FindBy(id = "result_box")
    WebElement result_box;

    public void translate(String text) {
        source.sendKeys(text, Keys.ENTER);
    }

    public String getResult() {
        return result_box.getAttribute("innerText");
    }


}
